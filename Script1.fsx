﻿
#light
#r @"c:\Users\matteo-win\Documents\Visual Studio 2010\Projects\VisualFSharp\Curve\bin\Debug\Curve.dll";;
open System.Windows.Forms;;
open System.Drawing;;
open System.Drawing.Drawing2D;;
open Curve;;

Application.EnableVisualStyles() ;;

let dx = 500;
let dy = 500;

let dWinSize = Size(dx,dy);;
let cWinSize = Size(200,400);;

let f(x,y) = Point(int(round x), int(round y));;

type operation = None | Rotation | Multiplication | HorizontalReflx | VerticalReflx | Traslation;;

let mutable mainCurve = Curve.point (0.0,0.0) + Curve.point(0.0,-100.0);;
let mutable currentOp = None;;

let dWin = new Form(Text="Drawing Window", Size=dWinSize);;
let cWin = new Form(Text="Control Window", Size=cWinSize);;
// flpanel is a container of buttons
let flpanel = new FlowLayoutPanel(Dock=DockStyle.Fill,BackColor=Color.AliceBlue, FlowDirection=FlowDirection.TopDown);;
let button1 = new Button(Text="|^");;
let button2 = new Button(Text="*");;
let button3 = new Button(Text="><");;
let button4 = new Button(Text="Vertical reflx");;
let button5 = new Button(Text="Traslation");;
let textbox1 = new TextBox(Text="0");;
let textbox2 = new TextBox(Text="1");;
let textbox3 = new TextBox(Text="0");;
let textbox4 = new TextBox(Text="0");;
let textbox5_1 = new TextBox(Text="0");;
let textbox5_2 = new TextBox(Text="0");;
let drawingPanel = new Panel(BackColor=Color.White);;
//drawingPanel.BackColor<-Color.Aqua;;
drawingPanel.Dock<-DockStyle.Fill;;

//creating a paint
let pen = new Pen(Color.Black);;

let center (x,y) = x + (float dx) / 2.0,y + (float dy) / 2.0

let drawCurve (g:Graphics) c = 
    let array = Curve.toList c |> List.map center |> List.map f |> Array.ofList; 
    //g.Clear(Color.White);
    g.DrawLines(pen, array);;

//creating handlers for buttons
let run cop (g:Graphics) = 
    match cop with
    | None -> ()
    | Rotation -> mainCurve <- mainCurve |^ (int textbox1.Text); drawCurve g mainCurve; ()
    | Multiplication -> mainCurve <-(float textbox2.Text) * mainCurve; drawCurve g mainCurve; ()
    | HorizontalReflx -> mainCurve <- mainCurve >< (float textbox3.Text); drawCurve g mainCurve; ()
    | VerticalReflx -> mainCurve <- verticRefl mainCurve (float textbox4.Text); drawCurve g mainCurve; ()
    | Traslation -> mainCurve <- mainCurve --> ((float textbox5_1.Text),(float textbox5_2.Text)); drawCurve g mainCurve; ()
    ;;
 
let handler = PaintEventHandler(fun obj pea -> run currentOp pea.Graphics);;
drawingPanel.Paint.AddHandler handler;;

//making buttons react to click events
button1.Click.Add(fun e -> currentOp <- Rotation; dWin.Refresh ());;
button2.Click.Add(fun e -> currentOp <- Multiplication; dWin.Refresh ());;
button3.Click.Add(fun e -> currentOp <- HorizontalReflx; dWin.Refresh ());;
button4.Click.Add(fun e -> currentOp <- VerticalReflx; dWin.Refresh ());;
button5.Click.Add(fun e -> currentOp <- Traslation; dWin.Refresh ());;

//adding buttons to container
flpanel.Controls.Add(button1);;
flpanel.Controls.Add(textbox1);;
flpanel.Controls.Add(button2);;
flpanel.Controls.Add(textbox2);;
flpanel.Controls.Add(button3);;
flpanel.Controls.Add(textbox3);;
flpanel.Controls.Add(button4);;
flpanel.Controls.Add(textbox4);;
flpanel.Controls.Add(button5);;
flpanel.Controls.Add(textbox5_1);;
flpanel.Controls.Add(textbox5_2);;

//adding container to window
cWin.Controls.Add(flpanel);;
dWin.Controls.Add(drawingPanel);;

//showing the result
dWin.Show();;
cWin.Show();;
